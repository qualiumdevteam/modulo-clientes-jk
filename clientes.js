var app = {};
(function($){
	$(document).ready(function(){

		$('.cust-list').click(function(){
			$('.cust-list').removeClass('currentActive');
			$(this).toggleClass('currentActive');

			var content = $(this).data('content');
			$('.content-clientes').removeClass('see-content');
			$('#'+content).toggleClass('see-content');

		});
 
 		app.modelo = Backbone.Model.extend({});

		var coleccion = Backbone.Collection.extend({
			url : '/jkguayaberas/wp-admin/admin-ajax.php?action=api_jk',
			model : app.modelo
		});

		app.copyCollection = new coleccion();

		app.modalView = Backbone.View.extend({
			tagName : 'div',
			className : 'eldetalle',
			template : _.template( $('#modaldatos').html() ),
			events : {
				'click .savedatos' : 'save',
			},

			initialize : function(){
				this.listenTo(this.model, 'change', this.render);
			},
			render : function(){				
				this.$el.html( this.template( this.model.toJSON() ) );
				this.$el.attr( 'id', 'modal'+this.model.get('id') );
				return this;
			},
			save : function(){
				
				var input = $('.eldetalle input');
				var tipo=5;
				var self=this;

				if( $(input[8]).is(':checked') )
				{
					tipo = $(input[8]).data('tipo');
				}
				if( $(input[9]).is(':checked') )
				{
					tipo = $(input[9]).data('tipo');
				}
				
				if( tipo != this.model.get('tipo_cliente')){
					$('.trc'+this.model.get('id')).remove();
				}

				this.model.save(
				{
					nombre 			: $(input[0]).val(),
					apellido 		: $(input[1]).val(),
					telefono 	  	: $(input[2]).val(),
					correo 		  	: $(input[3]).val(),
					codigo_postal	: $(input[4]).val(),
					direccion 		: $(input[5]).val(),
					ciudad 			: $(input[6]).val(),
					estado			: $(input[7]).val(),
					tipo_cliente	: tipo				
				},
				{
					wait:true,					
					success: function (exito){
						
					}, 
					error: function (error){}
				}
				);
			},
		});
		app.singleView = Backbone.View.extend({

			tagName : 'tr',
			template : _.template( $('#tpl-distribuidor').html() ),
			
			events : {
				'click .masinfo' : 'infocliente',
				'click .removecliente' : 'eliminar',
			},

			initialize : function(){
				this.listenTo(this.model, 'change', this.render);
				this.listenTo(this.model, 'destroy', this.remove); 
			},

			render : function(){
				this.$el.html( this.template( this.model.toJSON() ) );
				this.$el.addClass( 'trc'+this.model.get('id')+' ' );
				this.$el.attr('data-tipo',this.model.get('id'));
				return this;
			},
			infocliente : function(){				
				if( $('.cliente').attr('id') != 'modal'+this.model.get('id') ){
					var modal = new app.modalView({ model : this.model }).render().el;
					$('.datosdelcliente').html( modal );	
					$('.datosdelcliente').toggleClass('showdatos');
				}else{
					$('.datosdelcliente').toggleClass('showdatos');
				}
			},			
			
			eliminar : function(){
				var self = this;
				$('.datosdelcliente').toggleClass('showdatos');
				$('.alertaremove').toggleClass('showdatos');
			
				$('.aceptar').click(function(){
					self.model.destroy();					
				});
			}
		});

		app.masterView = Backbone.View.extend({
			el : '.content-clientes',
			events : {
				'click .closedatos' : 'ocultar',
				'click .cancelar' : 'togle'
			},
			
			initialize : function(){
				this.listenTo( app.copyCollection, 'add', this.list ); 
				this.listenTo( app.copyCollection, 'change', this.list ); 
				app.copyCollection.fetch();
				this.cliente;
				this.distribuidor;
			},

			render : function(){},

			list : function(elCliente)
			{
				this.cliente = new app.singleView({ model: elCliente });
				
				var who = elCliente.get('tipo_cliente') == 1 ? '.distribuidores-list' : '.enlinea-list';
					
				this.$(who).append( this.cliente.render().el );
			},

			lists : function(){
				app.copyCollection.each(this.list, this);
			},

			ocultar : function(event){
				
				$('.datosdelcliente').toggleClass('showdatos');
			},

			togle : function(){
				$('.datosdelcliente').toggleClass('showdatos');
				$('.alertaremove').toggleClass('showdatos');
			}
		});
		app.vistaenHtml = new app.masterView();

	}); /* Document Query */
	
})( jQuery );