<?php 
/*
Plugin Name: Panel Clientes
Plugin URI: http://jkGuayaberas.com
Description: Panel para mostrar a los clientes
Version: 1.0
Author: Alberto Qualium
Author URI: JKGuayaberas
License: GPLv2
*/
define('CONTOVER_PATH', plugin_dir_path(__FILE__));
define('CONTOVER_SLUG', 'clientes');
define('CONTOVER_URI', get_bloginfo('url') . '/wp-content/plugins/' . CONTOVER_SLUG);

add_action('admin_print_scripts', 'clientesjs');

function clientesjs(){
	
	$url = sprintf("%s%s%s","http://",$_SERVER['HTTP_HOST'],$_SERVER['REQUEST_URI']);
	
	if($url == 'http://localhost/qualium/jkguayaberas/wp-admin/admin.php?page=clientes')
	{
	    wp_enqueue_script( 'admin-clientesjs', CONTOVER_URI . '/clientes.js', array( 'jquery','underscore','backbone' ) );
	    wp_enqueue_style( 'admin-clientescss', CONTOVER_URI .'/clientes.css' );
	}
}

add_action('admin_menu', 'my_plugin_menu');
function my_plugin_menu() 
{
    add_menu_page( 'Clientes', 'Clientes', 'edit_posts', 'clientes', 'my_plugin_options' ); 
}

function my_plugin_options() {
	if (!current_user_can('edit_posts'))  {
		wp_die( __('No tiene suficientes permisos para acceder a esta página.') );
	} 
?>
	<div class="wrap">
        <h1>Clientes</h1>
        <div class="row">
        	
        	<ul class="my-customers">
        		<li  class="cust-list currentActive" data-content="tipo1">Distribuidores</li>
        		<li  class="cust-list" data-content="tipo2">Clientes En Línea</li>
        	</ul>

        	<div class="content-clientes">
        		
	        	<div id="tipo1" class="content-clientes see-content">
	        		<?php include 'distribuidores.php'; ?>
	        	</div>
	        	<div id="tipo2" class="content-clientes">
	        		<?php include 'clientesonline.php'; ?>
	        	</div>

	        	<div class="datosdelcliente">
	        		<div class="alertaremove">
	        			<h3>¿Seguro que desea eliminar al cliente?</h3>
	        			<div class="aceptar">Aceptar</div>
	        			<div class="cancelar">Cancelar</div>
	        		</div>
	        	</div>
	        	
        	</div>

        </div>
	</div>
	<!-- <div id="modal" class="cliente"></div> -->
	<script id="modaldatos" type="text/template">
		<h3>Cliente <%- nombre %></h3>
		<span class="closedatos">x</span>
		<p>
			<label>Nombre</label>
			<input type="text" value="<%- nombre %>">
		</p>
		<p>
			<label>Apellido</label>
			<input type="text" value="<%- apellido %>">
		</p>
		<p>
			<label>Teléfono</label>
			<input type="text" value="<%- telefono %>">
		</p>
		<p>
			<label>Correo</label>
			<input type="text" value="<%- correo %>">
		</p>
		<p>
			<label>Código Postal</label>
			<input type="text" value="<%- codigo_postal %>">
		</p>
		<p>
			<label>Dirección</label>
			<input type="text" value="<%- direccion %>">
		</p>
		<p>
			<label>Ciudad</label>
			<input type="text" value="<%- ciudad %>">
		</p>
		<p>
			<label>Estado</label>
			<input type="text" value="<%- estado %>">
		</p>				
		<p><br>
			<label>Cambiar a proveedor</label>
			<% var check1 = ( ( tipo_cliente==1)?'checked':'' ); %>
			<% var check2 = ( ( tipo_cliente==2)?'checked':'' ); %>
			<input data-tipo="1" type="radio" name="dist" <%- check1 %> >
			<input data-tipo="2" type="radio" name="dist" <%- check2 %> >
		</p>
		<div style="clear:both;"></div>
		<div class="savedatos">Guardar</div>
	</script>
	<script id="tpl-distribuidor" type="text/template">
	
		<th colspan="2" scope="col" class="manage-column column-cb check-column">
		<%- nombre %></th>
		<td colspan="2"> <%- correo %> </td>
		<td> <%- telefono %>  </td> 
		<td colspan="2"> <%- direccion %>  </td> 
		<td colspan="2"> 
			<div class="masinfo">Más Información</div>
			<div class="removecliente">Eliminar</div> 
		</td>
	</script>
<?php } ?>